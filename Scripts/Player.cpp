#include "Player.hpp"
#include "../LevelManager.hpp"

#include <Urho3D/Physics/CollisionShape.h>
#include <Urho3D/Graphics/Renderer.h>
#include <Urho3D/Graphics/Viewport.h>
#include <Urho3D/Graphics/Camera.h>
#include <Urho3D/Graphics/Octree.h>
#include <Urho3D/Graphics/OctreeQuery.h>
#include <Urho3D/Graphics/ReflectionProbe.h>
#include <Urho3D/Audio/Audio.h>
#include <Urho3D/Audio/SoundListener.h>
#include <Urho3D/UI/UI.h>



namespace Game {
void Player::Start() {
    levelManager = GetGlobalVar("LevelManager").GetCustom<LevelManager*>();
    body = GetNode()->GetChild("Body");
    camera = GetNode()->GetChild("Camera")->GetComponent<Camera>();
    camRelPos = camera->GetNode()->GetWorldPosition() - body->GetWorldPosition();

    auto* renderer = GetSubsystem<Renderer>();

    // Set viewport camera
    SharedPtr<Viewport> viewport(new Viewport(context_, GetScene(), camera));
    renderer->SetViewport(0, viewport);

    // Set audio audio listener
    GetSubsystem<Audio>()->SetListener(GetNode()->GetComponent<SoundListener>());
}

void Player::FixedUpdate(float timeStep) {
    auto input = GetSubsystem<Input>();

    // Escape key handling
    if (input->GetKeyDown(Key::KEY_ESCAPE)) {
        exit(0);
    }
}

void Player::Update(float timeStep) {
    auto* input = GetSubsystem<Input>();
    auto mMove = input->GetMouseMove();

    // Body
    float speedup = 10.f * float(input->GetMouseButtonDown(MouseButtonFlags::Enum::MOUSEB_ANY));
    body->GetComponent<RigidBody>()->ApplyForce(Vector3(speedup, 0.f, -mMove.x_*2.5f));
    camera->GetNode()->SetWorldPosition(body->GetWorldPosition()+camRelPos);

    // Camera
    camera->GetNode()->LookAt(body->GetWorldPosition());
}
}
